# Task Two - Geolocation service using REST

***

## Table of contents

* [General info](#general-info)
  * [Features](#features)
* [Technologies](#technologies)
* [Setup](#setup)
* [Author](#author)

## General info

Implementation of an efficient REST service that will receive and save information <br>
about the position (geolocation) from a mobile device, e.g. a phone/GPS tracker.<br>
Example of device data format: {'deviceId':'123456','latitude': 4530444, 'longitude': 10034555}.

### Features

* Application does not work in the browser.
* Open Postman, click on + to open new tab,<br>
    * change HTTP method from GET to POST and enter request URL: http://localhost:8080
* Create user on endpoint "/api/users/register"
    * click on Body, select raw and JSON format<br>
    * format to create user:<br>
      {"username": "name",
      "email": "email@test.pl",
      "password": "password"}
    * put it in Body
* Sign in on endpoint "/api/users/signIn"
    * click on Authorization and change Type to Basic Auth<br>
    * enter login details
        * after logging in, you get an access "Bearer" token for the endpoints
        * copy token
* Send coordinates on endpoint "/api/devices"
    * click on Body, select raw and JSON format<br>
    * format to sending coordinates:<br>
      {"deviceId": "123456",
      "latitude": "4530444",
      "longitude": "10034555"}
    * put it in Body
    * click on Authorization and change Type to Bearer Token<br>
    * paste token here
* All other endpoints are unavailable
* There is validation when entering data

## Technologies

Project is created with:

* IntelliJ IDEA 2022.2.3 (Ultimate Edition)
* Postman version 10.5.6 [Link][3]
* Java JDK version 17.0.2 [Link][1]
* Maven version 3.8.6 [Link][4]
* org.springframework.boot version 2.6.4
* mysql version 8.0.31
* org.projectlombok 1.18.24
* org.junit.jupiter version 5.9.0
* com.auth0 version 4.2.1

## Setup

1. To run this project, install Java JDK from the link [JDK][1]

2. Install it locally using cmd:

* $ cd ../selectedDirectory
* $ git clone https://gitlab.com/ks-enigmatasks/tasktwo.git (with https)
* $ git clone git@gitlab.com:ks-enigmatasks/tasktwo.git (with ssh)<br>

3. Download and install MySqlInstaller version 8.0 from the website [Link][2]<br>

* Open MySqlWorkBench and create a user on localhost: 3306 with<br>
    * login: admin, password: admin123<br>
      Note: The database schema and tables will be created automatically


4. Open project in your IDE (Intellij recommended).
   * Reload Maven to install required libraries.<br>
     Note: The project uses external libraries that are downloaded from an external maven repository.



## Author

Kamil Stencel

[1]:https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html]

[2]:https://dev.mysql.com/downloads/installer/

[3]:https://www.postman.com/downloads/

[4]:https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.zip
