package kamil.stencel.tasktwo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TaskTwoApplicationTests {

    @Test
    @DisplayName("shouldPassIfSpringContextLoadCorrectly")
    void contextLoads() {
    }

}
