package kamil.stencel.tasktwo.service;

import kamil.stencel.tasktwo.data.dto.UserDto;
import kamil.stencel.tasktwo.data.entity.UserEntity;
import kamil.stencel.tasktwo.data.mapper.UserMapper;
import kamil.stencel.tasktwo.data.repository.UserRepository;
import kamil.stencel.tasktwo.exceptions.IncorrectPasswordException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameOrPasswordLengthException;
import kamil.stencel.tasktwo.exceptions.UserAlreadyExistsException;
import kamil.stencel.tasktwo.security.SecurityService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class UserServiceImplTest {

    private static final String correctUsernameLength = "usr";
    private static final String correctEmail = "user@test.pl";
    private static final String correctPasswordLength = "user";
    private static final int minUserNameLength = correctUsernameLength.length();
    private static final int minPasswordLength = correctPasswordLength.length();
    private final long testId = 1;
    private final String testUsername = "usr";
    private final String testEmail = "user@test.pl";
    private final String testPassword = "user";
    private final String testWrongPassword = "userOne";
    private final UserDto testUserDto = new UserDto(testId, testUsername, testEmail, testPassword);
    private final String basicHeader = "Basic ";
    @InjectMocks
    UserServiceImpl userService;
    @Mock
    UserRepository userRepository;
    @Mock
    SecurityService securityService;

    private static final List<UserDto> users = List.of(
            UserDto.builder().id(1).username("").email(correctEmail).password(correctPasswordLength).build(),
            UserDto.builder().id(2).username("x").email(correctEmail).password(correctPasswordLength).build(),
            UserDto.builder().id(3).username("xx").email(correctEmail).password(correctPasswordLength).build(),
            UserDto.builder().id(4).username("xxx").email(correctEmail).password(correctPasswordLength).build(),
            UserDto.builder().id(5).username(correctUsernameLength).email(correctEmail).password("").build(),
            UserDto.builder().id(6).username(correctUsernameLength).email(correctEmail).password("x").build(),
            UserDto.builder().id(7).username(correctUsernameLength).email(correctEmail).password("xx").build(),
            UserDto.builder().id(8).username(correctUsernameLength).email(correctEmail).password("xxx").build(),
            UserDto.builder().id(9).username(correctUsernameLength).email(correctEmail).password("xxxx").build());

    private static Stream<UserDto> usersWhoHaveUserNameLengthLessThanRequiredAndGoodPasswordLength() {
        return users.stream().filter(user -> user.getUsername().length() < minUserNameLength &&
                user.getPassword().length() >= minPasswordLength);
    }

    private static Stream<UserDto> usersWhoHavePasswordLengthLessThanRequiredAndGoodUserNameLength() {
        return users.stream().filter(user -> user.getPassword().length() < minPasswordLength &&
                user.getUsername().length() >= minUserNameLength);
    }

    public UserServiceImplTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("shouldRegisterUserWhenUserDtoOk")
    void registerUser_userDtoOk() throws UserAlreadyExistsException {
        //given
        when(userRepository.saveAndFlush(Mockito.any(UserEntity.class))).thenReturn(UserMapper.dtoToEntity(testUserDto));

        //when + then
        userService.registerUser(testUserDto);
    }

    @Test
    @DisplayName("shouldThrowUserAlreadyExistsExceptionWhenUserDtoOkAndUsernameExists")
    void registerUser_userDtoOk_usernameExists() {
        //given
        when(userRepository.existsByUsername(Mockito.anyString())).thenReturn(true);

        //when+then
        assertThrows(UserAlreadyExistsException.class, () -> userService.registerUser(testUserDto));
    }

    @Test
    @DisplayName("shouldSignInWhenUserExistInDatabase")
    void signIn_userExistInDatabase() throws IncorrectPasswordException, IncorrectUserNameException, IncorrectUserNameOrPasswordLengthException {
        //given
        when(userRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(UserMapper.dtoToEntity(testUserDto)));
        when(securityService.isPasswordCorrect(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        final String goodUserNameAndPassword = decodeAuthorizationHeader(testUsername, testPassword);

        //when+then
        userService.signIn(goodUserNameAndPassword);
    }

    @ParameterizedTest
    @MethodSource("usersWhoHaveUserNameLengthLessThanRequiredAndGoodPasswordLength")
    @DisplayName("shouldThrowIncorrectUserNameOrPasswordExceptionWhenWrongUserNameLengthAndGoodPassword")
    void signIn_incorrectUserNameLength_goodPassword(final UserDto userDto) {
        //given
        when(userRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(UserMapper.dtoToEntity(userDto)));
        final String incorrectUserNameAndGoodPasswordLength = decodeAuthorizationHeader(userDto.getUsername(), userDto.getPassword());

        //when+then
        assertThrows(IncorrectUserNameOrPasswordLengthException.class, () -> userService.signIn(incorrectUserNameAndGoodPasswordLength));
    }

    @ParameterizedTest
    @MethodSource("usersWhoHavePasswordLengthLessThanRequiredAndGoodUserNameLength")
    @DisplayName("shouldThrowIncorrectUserNameOrPasswordExceptionWhenGoodUserNameAndIncorrectPasswordLength")
    void signIn_goodUserName_incorrectPasswordLength(final UserDto userDto) {
        //given
        when(userRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(UserMapper.dtoToEntity(userDto)));
        final String goodUserNameAndIncorrectPasswordLength = decodeAuthorizationHeader(userDto.getUsername(), userDto.getPassword());

        //when+then
        assertThrows(IncorrectUserNameOrPasswordLengthException.class, () -> userService.signIn(goodUserNameAndIncorrectPasswordLength));
    }

    @Test
    @DisplayName("shouldThrowIncorrectUserNameExceptionWhenWrongUserNameAndGoodPassword")
    void signIn_incorrectUserName_goodPassword() {
        //given
        final String goodUserNameAndPasswordLength = decodeAuthorizationHeader(testUsername, testPassword);

        //when+then
        assertThrows(IncorrectUserNameException.class, () -> userService.signIn(goodUserNameAndPasswordLength));
    }

    @Test
    @DisplayName("shouldThrowIncorrectUserNameExceptionWhenGoodUserNameAndIncorrectPassword")
    void signIn_goodUserName_incorrectPassword() {
        //given
        when(userRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(UserMapper.dtoToEntity(testUserDto)));
        when(securityService.isPasswordCorrect(Mockito.anyString(), Mockito.anyString())).thenReturn(false);
        final String goodUserNameAndPasswordLength = decodeAuthorizationHeader(testUsername, testWrongPassword);

        //when+then
        assertThrows(IncorrectPasswordException.class, () -> userService.signIn(goodUserNameAndPasswordLength));
    }

    private String decodeAuthorizationHeader(final String username, final String password) {
        final String userNameAndPassword = String.format("%s:%s", username, password);
        final byte[] userNameAndPasswordBytes = userNameAndPassword.getBytes(StandardCharsets.UTF_8);
        final String encodedAuthorizationHeader = Base64.getEncoder().encodeToString(userNameAndPasswordBytes);
        return basicHeader + encodedAuthorizationHeader;
    }
}