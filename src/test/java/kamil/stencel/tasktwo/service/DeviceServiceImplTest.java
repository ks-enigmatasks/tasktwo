package kamil.stencel.tasktwo.service;

import kamil.stencel.tasktwo.data.form.DeviceForm;
import kamil.stencel.tasktwo.data.repository.DeviceRepository;
import kamil.stencel.tasktwo.exceptions.AccessDeniedException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectDeviceIdLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeFormatException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeOrLongitudeLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLongitudeFormatException;
import kamil.stencel.tasktwo.utils.JwtHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;

class DeviceServiceImplTest {

    private static final String correctDeviceId = "1";
    private static final String correctLatitude = "4530444";
    private static final String correctLongitude = "10030555";
    private static final int incorrectDeviceId = 0;
    private static final int maxDeviceIdLength = 6;
    private static final int minValue = 0;
    private static final int maxLatitude = 9000000;
    private static final int maxLongitude = 18000000;

    private final long testUserId = 1;
    private final String testGoodToken = "goodToken";
    private final String testMalformedToken = "malformedToken";
    private final String testAuthenticationToken = JwtHelper.INSTANCE.createAuthenticationToken(testUserId, testGoodToken);
    private final String testMalformedAuthenticationToken = JwtHelper.INSTANCE.createAuthenticationToken(testUserId, testMalformedToken);
    private final String bearerToken = "Bearer " + testAuthenticationToken;
    private final String testDeviceId = "1";
    private final String testWrongDeviceIdLength = "1234567";
    private final String testLatitude = "8959999";
    private final String testWrongLatitude = "8960999";
    private final String testWrongLatitudeLength = "12345";
    private final String testLongitude = "17959999";
    private final String testWrongLongitude = "17960999";
    private final String testWrongLongitudeLength = "1234567";
    private final DeviceForm testCorrectDeviceForm = new DeviceForm(testDeviceId, testLatitude, testLongitude);
    private final DeviceForm testWrongDeviceIdDeviceForm = new DeviceForm(testWrongDeviceIdLength, testLatitude, testLongitude);
    private final DeviceForm testWrongLatitudeLengthDeviceForm = new DeviceForm(testDeviceId, testWrongLatitudeLength, testLongitude);
    private final DeviceForm testWrongLatitudeDeviceForm = new DeviceForm(testDeviceId, testWrongLatitude, testLongitude);
    private final DeviceForm testWrongLongitudeLengthDeviceForm = new DeviceForm(testDeviceId, testLatitude, testWrongLongitudeLength);
    private final DeviceForm testWrongLongitudeDeviceForm = new DeviceForm(testDeviceId, testLatitude, testWrongLongitude);

    @InjectMocks
    DeviceServiceImpl deviceService;
    @Mock
    DeviceRepository deviceRepository;

    private static final List<DeviceForm> devicesFormList = List.of(
            new DeviceForm("1", correctLatitude, correctLongitude),
            new DeviceForm("999999", correctLatitude, correctLongitude),
            new DeviceForm(correctDeviceId, "0000000", correctLongitude),
            new DeviceForm(correctDeviceId, "8959999", correctLongitude),
            new DeviceForm(correctDeviceId, "9000000", correctLongitude),
            new DeviceForm(correctDeviceId, "9059999", correctLongitude),
            new DeviceForm(correctDeviceId, correctLatitude, "00000000"),
            new DeviceForm(correctDeviceId, correctLatitude, "17959999"),
            new DeviceForm(correctDeviceId, correctLatitude, "18000000"),
            new DeviceForm(correctDeviceId, correctLatitude, "18059999"));

    private static final Stream<DeviceForm> correctDevices() {
        return devicesFormList.stream().filter(device -> device.deviceId().length() <= maxDeviceIdLength &&
                device.deviceId().length() != incorrectDeviceId &&
                (Integer.parseInt(device.latitude()) >= minValue) &&
                (Integer.parseInt(device.latitude()) <= maxLatitude) &&
                (Integer.parseInt(device.longitude()) >= minValue) &&
                (Integer.parseInt(device.longitude()) <= maxLongitude)
        );
    }

    public DeviceServiceImplTest() {
        MockitoAnnotations.openMocks(this);
    }

    @ParameterizedTest
    @MethodSource("correctDevices")
    @DisplayName("shouldSendCoordinatesWhenTokenOkAndDataInDeviceFormIsOk")
    void sendCoordinates_tokenOk_deviceFormOk(final DeviceForm deviceForm) throws
            AccessDeniedException, IncorrectLatitudeOrLongitudeLengthException,
            IncorrectLatitudeFormatException, IncorrectLongitudeFormatException,
            IncorrectDeviceIdLengthException {
        //given
        final var correctDevice = new DeviceForm(deviceForm.deviceId(), deviceForm.latitude(), deviceForm.longitude());

        //when + then
        deviceService.sendCoordinates(correctDevice, bearerToken);
    }

    @Test
    @DisplayName("shouldThrowAccessDeniedExceptionWhenTokenMalformedAndDataInDeviceFormIsOk")
    void sendCoordinates_tokenMalformed_deviceFormOk() {
        //given

        //when + then
        assertThrows(AccessDeniedException.class, () -> deviceService.sendCoordinates(testCorrectDeviceForm, testMalformedAuthenticationToken));
    }

    @Test
    @DisplayName("shouldThrowIncorrectDeviceIdLengthExceptionWhenTokenOkAndIncorrectDeviceIdLength")
    void sendCoordinates_tokenOk_incorrectDeviceIdLength_inDeviceForm() {
        //given

        //when + then
        assertThrows(IncorrectDeviceIdLengthException.class, () -> deviceService.sendCoordinates(testWrongDeviceIdDeviceForm, bearerToken));
    }

    @Test
    @DisplayName("shouldThrowIncorrectLatitudeOrLongitudeLengthExceptionWhenTokenOkAndIncorrectLatitudeLength")
    void sendCoordinates_tokenOk_incorrectLatitudeLength_inDeviceForm() {
        //given

        //when + then
        assertThrows(IncorrectLatitudeOrLongitudeLengthException.class, () -> deviceService.sendCoordinates(testWrongLatitudeLengthDeviceForm, bearerToken));
    }

    @Test
    @DisplayName("shouldThrowIncorrectLatitudeOrLongitudeLengthExceptionWhenTokenOkAndIncorrectLongitudeLength")
    void sendCoordinates_tokenOk_incorrectLongitudeLength_inDeviceForm() {
        //given

        //when + then
        assertThrows(IncorrectLatitudeOrLongitudeLengthException.class, () -> deviceService.sendCoordinates(testWrongLongitudeLengthDeviceForm, bearerToken));
    }

    @Test
    @DisplayName("shouldThrowIncorrectLatitudeFormatExceptionWhenTokenOkAndIncorrectLatitudeFormat")
    void sendCoordinates_tokenOk_incorrectLatitudeFormat_inDeviceForm() {
        //given

        //when + then
        assertThrows(IncorrectLatitudeFormatException.class, () -> deviceService.sendCoordinates(testWrongLatitudeDeviceForm, bearerToken));
    }

    @Test
    @DisplayName("shouldThrowIncorrectLongitudeFormatExceptionWhenTokenOkAndIncorrectLongitudeFormat")
    void sendCoordinates_tokenOk_incorrectLongitudeFormat_inDeviceForm() {
        //given

        //when + then
        assertThrows(IncorrectLongitudeFormatException.class, () -> deviceService.sendCoordinates(testWrongLongitudeDeviceForm, bearerToken));
    }
}