CREATE TABLE IF NOT EXISTS task_two.users
(
    id          bigint NOT NULL AUTO_INCREMENT primary key,
    username    varchar(50) NOT NULL,
    email       varchar(50) NOT NULL,
    password    varchar(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS task_two.devices
(
    id          bigint NOT NULL AUTO_INCREMENT primary key,
    device_id   varchar(6) NOT NULL,
    latitude    varchar(7) NOT NULL,
    longitude   varchar(8) NOT NULL,
    user_id     bigint NOT NULL
);