package kamil.stencel.tasktwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskTwoApplication {

    public static void main(String[] args) {
        System.out.println("Kamil Stencel, Enigma taskTwo");
        SpringApplication.run(TaskTwoApplication.class, args);
    }

}
