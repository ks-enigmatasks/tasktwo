package kamil.stencel.tasktwo.security;

public interface SecurityService {
    String hashPassword(String password);

    boolean isPasswordCorrect(String passwordToCheck, String passwordFromDb);
}
