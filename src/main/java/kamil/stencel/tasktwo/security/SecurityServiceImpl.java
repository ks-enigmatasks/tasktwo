package kamil.stencel.tasktwo.security;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Override
    public String hashPassword(final String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(10));
    }

    @Override
    public boolean isPasswordCorrect(final String passwordToCheck, final String passwordFromDb) {
        return BCrypt.checkpw(passwordToCheck, passwordFromDb);
    }
}
