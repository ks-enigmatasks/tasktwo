package kamil.stencel.tasktwo.exceptions;

public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(final String message) {
        super(message);
    }
}
