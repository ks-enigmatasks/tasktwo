package kamil.stencel.tasktwo.exceptions;

public class IncorrectUserNameException extends Exception {

    public IncorrectUserNameException(final String message) {
        super(message);
    }
}
