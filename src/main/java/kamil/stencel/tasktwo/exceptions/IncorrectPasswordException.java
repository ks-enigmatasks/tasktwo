package kamil.stencel.tasktwo.exceptions;

public class IncorrectPasswordException extends Exception {

    public IncorrectPasswordException(final String message) {
        super(message);
    }
}
