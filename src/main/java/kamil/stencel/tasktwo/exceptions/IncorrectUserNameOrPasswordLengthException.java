package kamil.stencel.tasktwo.exceptions;

public class IncorrectUserNameOrPasswordLengthException extends Exception {

    public IncorrectUserNameOrPasswordLengthException(final String message) {
        super(message);
    }
}
