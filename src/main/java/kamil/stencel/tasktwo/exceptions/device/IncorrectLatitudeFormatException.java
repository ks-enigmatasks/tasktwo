package kamil.stencel.tasktwo.exceptions.device;

public class IncorrectLatitudeFormatException extends Exception {

    public IncorrectLatitudeFormatException(final String message) {
        super(message);
    }
}
