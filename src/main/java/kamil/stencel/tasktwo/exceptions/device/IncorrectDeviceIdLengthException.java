package kamil.stencel.tasktwo.exceptions.device;

public class IncorrectDeviceIdLengthException extends Exception {

    public IncorrectDeviceIdLengthException(final String message) {
        super(message);
    }
}
