package kamil.stencel.tasktwo.exceptions.device;

public class IncorrectLongitudeFormatException extends Exception {

    public IncorrectLongitudeFormatException(final String message) {
        super(message);
    }
}
