package kamil.stencel.tasktwo.exceptions.device;

public class IncorrectLatitudeOrLongitudeLengthException extends Exception {

    public IncorrectLatitudeOrLongitudeLengthException(final String message) {
        super(message);
    }
}
