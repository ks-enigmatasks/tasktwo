package kamil.stencel.tasktwo.exceptions;

public class AccessDeniedException extends Exception {

    public AccessDeniedException(final String message) {
        super(message);
    }
}
