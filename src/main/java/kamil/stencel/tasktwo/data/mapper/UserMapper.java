package kamil.stencel.tasktwo.data.mapper;

import kamil.stencel.tasktwo.data.dto.UserDto;
import kamil.stencel.tasktwo.data.entity.UserEntity;

public class UserMapper {

    public static UserDto entityToDto(final UserEntity userEntity) {
        return UserDto.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .email(userEntity.getEmail())
                .password(userEntity.getPassword())
                .build();
    }

    public static UserEntity dtoToEntity(final UserDto userDto) {
        return new UserEntity(userDto.getId(),
                userDto.getUsername(),
                userDto.getEmail(),
                userDto.getPassword());
    }
}
