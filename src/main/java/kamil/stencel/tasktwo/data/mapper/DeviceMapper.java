package kamil.stencel.tasktwo.data.mapper;

import kamil.stencel.tasktwo.data.dto.DeviceDto;
import kamil.stencel.tasktwo.data.entity.DeviceEntity;

public class DeviceMapper {

    public static DeviceEntity dtoToEntity(final DeviceDto deviceDto) {
        return new DeviceEntity(
                deviceDto.getId(),
                deviceDto.getDeviceId(),
                deviceDto.getLatitude(),
                deviceDto.getLongitude(),
                deviceDto.getUserId());
    }
}
