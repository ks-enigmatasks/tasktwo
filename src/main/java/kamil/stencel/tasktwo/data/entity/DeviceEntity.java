package kamil.stencel.tasktwo.data.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode

@Entity
@Table(name = "devices")
public class DeviceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "device_id", length = 6, nullable = false)
    private String deviceId;

    @Column(length = 7, nullable = false)
    private String latitude;

    @Column(length = 8, nullable = false)
    private String longitude;

    @Column(name = "user_id", nullable = false)
    private long userId;
}
