package kamil.stencel.tasktwo.data.form;

import javax.validation.constraints.*;

public record DeviceForm(
        @NotNull(message = "Device id has to be provided!")
        @NotEmpty(message = "Device id has to be provided!")
        @Min(value = 1, message = "Device id must be equal to or greater than 1")
        @Max(value = 999999, message = "Device id must be equal to or less than 999999")
        @Digits(integer = 6, fraction = 0, message = "Device id can only contain numbers!")
        String deviceId,
        @NotNull(message = "Latitude has to be provided!")
        @NotEmpty(message = "Latitude has to be provided!")
        @Min(value = 0, message = "Latitude has to be provided!")
        @Max(value = 9000000, message = "Latitude must be equal to or less than 9000000")
        @Digits(integer = 7, fraction = 0, message = "Latitude can only contain numbers!")
        String latitude,
        @NotNull(message = "Longitude has to be provided!")
        @NotEmpty(message = "Longitude has to be provided!")
        @Min(value = 0, message = "Longitude has to be provided!")
        @Max(value = 18000000, message = "Longitude must be equal to or less than 18000000")
        @Digits(integer = 8, fraction = 0, message = "Longitude can only contain numbers!")
        String longitude) {
}

