package kamil.stencel.tasktwo.data.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class UserDto {

    private long id;
    @NotNull(message = "Username has to be provided!")
    @NotEmpty(message = "Username has to be provided!")
    @Length(min = 3, max = 50, message = "Username has to have at least 3 characters and up to 50 characters!")
    private String username;
    @Email(message = "Invalid email address!")
    private String email;
    @NotNull(message = "Password has to be provided!")
    @NotEmpty(message = "Password has to be provided!")
    @Size(min = 4, message = "Password has to have at least 4 characters!")
    private String password;
}
