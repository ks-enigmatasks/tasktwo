package kamil.stencel.tasktwo.data.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
public class DeviceDto {

    private long id;
    private String deviceId;
    private String latitude;
    private String longitude;
    private long userId;
}
