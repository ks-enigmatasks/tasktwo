package kamil.stencel.tasktwo.utils;

import lombok.Getter;

public enum TokenDetails {
    USER_ID("userId"),
    USER_TOKEN("userToken");

    @Getter
    private final String key;

    TokenDetails(final String key) {
        this.key = key;
    }
}
