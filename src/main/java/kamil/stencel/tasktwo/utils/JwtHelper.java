package kamil.stencel.tasktwo.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Date;
import java.util.Map;


public enum JwtHelper {
    INSTANCE;
    private static final Logger logger = LoggerFactory.getLogger(JwtHelper.class);
    private final String issuer;
    private final Algorithm algorithm;

    JwtHelper() {
        this.issuer = "Kamil Stencel";
        this.algorithm = Algorithm.HMAC256("1-2+3-4+5&6@7&8@9");
    }

    public String createAuthenticationToken(final long userId, final String userToken) {
        final var currentTime = System.currentTimeMillis();
        return JWT.create()
                .withIssuer(issuer)
                .withClaim(TokenDetails.USER_ID.getKey(), userId)
                .withClaim(TokenDetails.USER_TOKEN.getKey(), userToken)
                .withExpiresAt(new Date(currentTime + (1000 * 60 * 5)))
                .sign(algorithm);
    }

    public Map<String, Claim> decodeAuthenticationToken(final String authenticationToken) {
        try {
            final JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(issuer)
                    .build();
            final DecodedJWT decodedJwt = verifier.verify(authenticationToken);
            return decodedJwt.getClaims();
        } catch (JWTVerificationException exc) {
            logger.warn("{}", exc.getMessage());
        }
        return Collections.emptyMap();
    }
}
