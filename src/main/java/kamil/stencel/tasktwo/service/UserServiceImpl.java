package kamil.stencel.tasktwo.service;

import kamil.stencel.tasktwo.data.dto.UserDto;
import kamil.stencel.tasktwo.data.entity.UserEntity;
import kamil.stencel.tasktwo.data.mapper.UserMapper;
import kamil.stencel.tasktwo.data.repository.UserRepository;
import kamil.stencel.tasktwo.exceptions.IncorrectPasswordException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameOrPasswordLengthException;
import kamil.stencel.tasktwo.exceptions.UserAlreadyExistsException;
import kamil.stencel.tasktwo.security.SecurityService;
import kamil.stencel.tasktwo.utils.JwtHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final String BASIC_HEADER = "Basic ";
    private final int USERNAME = 0;
    private final int PASSWORD = 1;

    private final UserRepository userRepository;
    private final SecurityService securityService;

    public UserServiceImpl(final UserRepository userRepository, final SecurityService securityService) {
        this.userRepository = userRepository;
        this.securityService = securityService;
    }

    @Override
    public String registerUser(final UserDto userDto) throws UserAlreadyExistsException {
        if (!checkIfUserNameExists(userDto.getUsername())) {
            final UserEntity newUser = new UserEntity(userDto.getId(), userDto.getUsername(),
                    userDto.getEmail(), securityService.hashPassword(userDto.getPassword()));
            final UserDto savedUser = UserMapper.entityToDto(userRepository.saveAndFlush(newUser));
            logger.info("'{}' successfully registered!", savedUser.getUsername());
            return savedUser.getUsername();
        } else {
            logger.warn("User '{}' already exists!", userDto.getUsername());
            throw new UserAlreadyExistsException("User already exists!");
        }
    }

    @Override
    public String signIn(final String authorizationHeader) throws IncorrectUserNameException, IncorrectUserNameOrPasswordLengthException, IncorrectPasswordException {
        final var decodedAuthHeader = Base64.getDecoder().decode(authorizationHeader.substring(BASIC_HEADER.length()));
        if (decodedAuthHeader.length < 8) {
            logger.warn("User provided wrong username or password");
            throw new IncorrectUserNameOrPasswordLengthException("Bad username or password!");
        }
        final String[] decodedTab = new String(decodedAuthHeader).split(":");
        final var username = decodedTab[USERNAME];
        final var password = decodedTab[PASSWORD];
        logger.info("Logging in with username: '{}'", username);

        if (checkUserNameAndPassword(username, password)) {
            logger.info("Logged in with username: '{}'", username);
        }
        final UserDto userDto = UserMapper.entityToDto(userRepository.findByUsername(username).get());
        final var userToken = generateUniqueUserToken();
        return JwtHelper.INSTANCE.createAuthenticationToken(userDto.getId(), userToken);
    }

    private boolean checkUserNameAndPassword(final String username, final String password) throws IncorrectUserNameException, IncorrectPasswordException {
        final UserDto userDto = UserMapper.entityToDto(userRepository.findByUsername(username)
                .orElseThrow(() -> {
                    logger.warn("Bad username: '{}'", username);
                    return new IncorrectUserNameException("Bad username or password!");
                }));
        if (!securityService.isPasswordCorrect(password, userDto.getPassword())) {
            logger.warn("Bad password: '{}'", password);
            throw new IncorrectPasswordException("Bad username or password!");
        }
        return true;
    }

    private boolean checkIfUserNameExists(final String username) {
        return userRepository.existsByUsername(username);
    }

    private String generateUniqueUserToken() {
        return UUID.randomUUID().toString();
    }
}
