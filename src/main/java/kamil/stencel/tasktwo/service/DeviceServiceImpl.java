package kamil.stencel.tasktwo.service;

import kamil.stencel.tasktwo.data.dto.DeviceDto;
import kamil.stencel.tasktwo.data.form.DeviceForm;
import kamil.stencel.tasktwo.data.mapper.DeviceMapper;
import kamil.stencel.tasktwo.data.repository.DeviceRepository;
import kamil.stencel.tasktwo.exceptions.AccessDeniedException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectDeviceIdLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeFormatException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeOrLongitudeLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLongitudeFormatException;
import kamil.stencel.tasktwo.utils.JwtHelper;
import kamil.stencel.tasktwo.utils.TokenDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DeviceServiceImpl implements DeviceService {

    private static final Logger logger = LoggerFactory.getLogger(DeviceServiceImpl.class);

    private final int DEVICE_ID_CORRECT_LENGTH = 6;
    private final int LATITUDE_CORRECT_LENGTH = 7;
    private final int LONGITUDE_CORRECT_LENGTH = 8;
    private final int MIN_VALUE = 0;
    private final int MAX_LATITUDE_DEGREES = 90;
    private final int MAX_LONGITUDE_DEGREES = 180;
    private final int MAX_MINUTES = 60;
    private final int MAX_SECONDS = 999;

    private final String BEARER_HEADER = "Bearer ";
    private final DeviceRepository deviceRepository;

    public DeviceServiceImpl(final DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public void sendCoordinates(final DeviceForm deviceForm, final String AuthorizationToken) throws AccessDeniedException,
            IncorrectLatitudeOrLongitudeLengthException, IncorrectLatitudeFormatException, IncorrectLongitudeFormatException,
            IncorrectDeviceIdLengthException {
        final var token = AuthorizationToken.substring(BEARER_HEADER.length());
        final var claimFromToken = JwtHelper.INSTANCE.decodeAuthenticationToken(token);
        if (claimFromToken.isEmpty()) {
            throw new AccessDeniedException("Access denied! Token malformed or expired!");
        }
        final var userId = claimFromToken.get(TokenDetails.USER_ID.getKey()).asLong();
        final var deviceId = checkDeviceIdCorrectness(deviceForm.deviceId());
        final var latitude = checkLatitudeCorrectness(deviceForm.latitude());
        final var longitude = checkLongitudeCorrectness(deviceForm.longitude());
        final var deviceDto = createDevice(deviceId, latitude, longitude, userId);

        deviceRepository.saveAndFlush(DeviceMapper.dtoToEntity(deviceDto));
        logger.info("User with id '{}' send new coordinates '{}','{}'", userId, deviceDto.getLatitude(), deviceDto.getLongitude());
    }

    private DeviceDto createDevice(final String deviceId, final String latitude, final String longitude, final long userId) {
        return DeviceDto.builder()
                .deviceId(deviceId)
                .latitude(latitude)
                .longitude(longitude)
                .userId(userId)
                .build();
    }

    private String checkDeviceIdCorrectness(final String deliveredDeviceId) throws IncorrectDeviceIdLengthException {
        final int length = deliveredDeviceId.length();
        final String repeater = "0";
        if (length < DEVICE_ID_CORRECT_LENGTH) {
            final String correctDeviceId = repeater.repeat((DEVICE_ID_CORRECT_LENGTH - length));
            return String.format("%s%s", correctDeviceId, deliveredDeviceId);
        }
        if (length > DEVICE_ID_CORRECT_LENGTH) {
            logger.warn("Incorrect device id length, 6 digits maximum required!");
            throw new IncorrectDeviceIdLengthException("Wrong device id length!");
        }
        return deliveredDeviceId;
    }

    private String checkLatitudeCorrectness(final String deliveredLatitude) throws
            IncorrectLatitudeOrLongitudeLengthException, IncorrectLatitudeFormatException {
        final int length = deliveredLatitude.length();
        if (length != LATITUDE_CORRECT_LENGTH) {
            logger.warn("Incorrect latitude length!");
            throw new IncorrectLatitudeOrLongitudeLengthException("Wrong latitude length!");
        }
        final int degrees = Integer.parseInt(deliveredLatitude.substring(0, 2));
        final int minutes = Integer.parseInt(deliveredLatitude.substring(2, 4));
        final int seconds = Integer.parseInt(deliveredLatitude.substring(4, 7));
        if (degrees < MIN_VALUE || degrees > MAX_LATITUDE_DEGREES) {
            throw new IncorrectLatitudeFormatException("'Latitude' Wrong degrees format, numerical interval <0,90>");
        }
        if (minutes < MIN_VALUE || minutes >= MAX_MINUTES) {
            throw new IncorrectLatitudeFormatException("'Latitude' Wrong minutes format, numerical interval <0,60)");
        }
        if (seconds < MIN_VALUE || seconds > MAX_SECONDS) {
            throw new IncorrectLatitudeFormatException("'Latitude' Wrong seconds format, numerical interval <0,999>");
        }
        return deliveredLatitude;
    }

    private String checkLongitudeCorrectness(final String deliveredLongitude) throws
            IncorrectLatitudeOrLongitudeLengthException, IncorrectLongitudeFormatException {
        final int length = deliveredLongitude.length();
        if (length != LONGITUDE_CORRECT_LENGTH) {
            logger.warn("Incorrect longitude length!");
            throw new IncorrectLatitudeOrLongitudeLengthException("Wrong longitude length!");
        }
        final int degrees = Integer.parseInt(deliveredLongitude.substring(0, 3));
        final int minutes = Integer.parseInt(deliveredLongitude.substring(3, 5));
        final int seconds = Integer.parseInt(deliveredLongitude.substring(5, 8));
        if (degrees < MIN_VALUE || degrees > MAX_LONGITUDE_DEGREES) {
            throw new IncorrectLongitudeFormatException("'Longitude' Wrong degrees format, numerical interval <0,180>");
        }
        if (minutes < MIN_VALUE || minutes >= MAX_MINUTES) {
            throw new IncorrectLongitudeFormatException("'Longitude' Wrong minutes format, numerical interval <0,60)");
        }
        if (seconds < MIN_VALUE || seconds > MAX_SECONDS) {
            throw new IncorrectLongitudeFormatException("'Longitude' Wrong seconds format, numerical interval <0,999>");
        }
        return deliveredLongitude;
    }
}
