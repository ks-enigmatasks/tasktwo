package kamil.stencel.tasktwo.service;

import kamil.stencel.tasktwo.data.form.DeviceForm;
import kamil.stencel.tasktwo.exceptions.AccessDeniedException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectDeviceIdLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeFormatException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeOrLongitudeLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLongitudeFormatException;

public interface DeviceService {
    void sendCoordinates(DeviceForm deviceForm, String AuthorizationToken) throws AccessDeniedException,
            IncorrectLatitudeOrLongitudeLengthException, IncorrectLatitudeFormatException, IncorrectLongitudeFormatException, IncorrectDeviceIdLengthException;
}
