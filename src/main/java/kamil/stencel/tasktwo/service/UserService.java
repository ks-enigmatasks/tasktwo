package kamil.stencel.tasktwo.service;

import kamil.stencel.tasktwo.data.dto.UserDto;
import kamil.stencel.tasktwo.exceptions.IncorrectPasswordException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameOrPasswordLengthException;
import kamil.stencel.tasktwo.exceptions.UserAlreadyExistsException;


public interface UserService {

    String registerUser(UserDto userDto) throws UserAlreadyExistsException;

    String signIn(String authorizationHeader) throws IncorrectUserNameException, IncorrectUserNameOrPasswordLengthException, IncorrectPasswordException;
}
