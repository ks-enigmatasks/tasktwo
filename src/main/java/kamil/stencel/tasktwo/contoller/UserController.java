package kamil.stencel.tasktwo.contoller;

import kamil.stencel.tasktwo.data.dto.UserDto;
import kamil.stencel.tasktwo.exceptions.IncorrectPasswordException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameException;
import kamil.stencel.tasktwo.exceptions.IncorrectUserNameOrPasswordLengthException;
import kamil.stencel.tasktwo.exceptions.UserAlreadyExistsException;
import kamil.stencel.tasktwo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("api/users")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> registerUser(@Valid @RequestBody final UserDto userDto)
            throws UserAlreadyExistsException {
        logger.info("Registering user with username: '{}'", userDto.getUsername());
        return ResponseEntity.status(HttpStatus.OK)
                .body(String.format("Successfully registered user: '%s'", userService.registerUser(userDto)));
    }

    @PostMapping(value = "/signIn", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> signInUser(final HttpServletRequest request)
            throws IncorrectUserNameOrPasswordLengthException,
            IncorrectUserNameException, IncorrectPasswordException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(userService.signIn(request.getHeader("Authorization")));
    }
}
