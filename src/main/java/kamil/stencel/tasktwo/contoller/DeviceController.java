package kamil.stencel.tasktwo.contoller;

import kamil.stencel.tasktwo.data.form.DeviceForm;
import kamil.stencel.tasktwo.exceptions.AccessDeniedException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectDeviceIdLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeFormatException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeOrLongitudeLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLongitudeFormatException;
import kamil.stencel.tasktwo.service.DeviceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/devices")
public class DeviceController {

    private final DeviceService deviceService;

    public DeviceController(final DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @PostMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> sendCoordinates(@Valid @RequestBody DeviceForm deviceForm,
                                                  final HttpServletRequest request) throws
            AccessDeniedException, IncorrectLatitudeOrLongitudeLengthException,
            IncorrectLatitudeFormatException, IncorrectLongitudeFormatException,
            IncorrectDeviceIdLengthException {
        deviceService.sendCoordinates(deviceForm, request.getHeader("Authorization"));
        return ResponseEntity.status(HttpStatus.OK).body("Coordinates successfully sent!");
    }
}
