package kamil.stencel.tasktwo.contoller;

import kamil.stencel.tasktwo.exceptions.*;
import kamil.stencel.tasktwo.exceptions.device.IncorrectDeviceIdLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeFormatException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLatitudeOrLongitudeLengthException;
import kamil.stencel.tasktwo.exceptions.device.IncorrectLongitudeFormatException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler({UserAlreadyExistsException.class, IncorrectUserNameOrPasswordLengthException.class,
            IncorrectUserNameException.class, IncorrectPasswordException.class})
    public ResponseEntity<String> userExcHandler(Exception exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getMessage());
    }

    @ExceptionHandler({AccessDeniedException.class, IncorrectLatitudeOrLongitudeLengthException.class,
            IncorrectLatitudeFormatException.class, IncorrectLongitudeFormatException.class,
            IncorrectDeviceIdLengthException.class})
    public ResponseEntity<String> deviceExcHandler(Exception exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> wrongUserInputExcHandler(MethodArgumentNotValidException exc) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exc.getFieldError().getDefaultMessage());
    }
}
